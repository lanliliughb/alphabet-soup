/* 
*  The following code was developed and tested under node.js
*  How to run:
*  1. Save the data in a text file such as "input.txt"
*  2. Run node.js to read and process the file using the following command:
*     node ./lanli_code.js input.txt
*/

// Make sure we got a filename on the command line.
if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' FILENAME');
    process.exit(1);
  }
  // Read the file line by line and process and print the result in the end
  var fs = require('fs')
    , filename = process.argv[2];
  
  const readline = require('readline'); 
  
  const file = readline.createInterface({ 
      input: fs.createReadStream(filename), 
      output: process.stdout, 
      terminal: false
  }); 
  
  const searchWords = [],
          data = [];
  
  let ct = 0;
  let rowTot = colTot = 0;
  
  file.on('line', (line) => { 
      ct++;
      if(ct==1){
          let splitted = line.split("x");
          rowTot = 1*splitted[0];
          colTot = 1*splitted[1];
      }else{
          if(ct>1 && ct<= (rowTot+1)){
              data.push(line.split(" "));
          }else{
              searchWords.push(line);
          }        
      }
  });
  
  //process data after all lines are read
  file.input.on("end", ()=>{
      searchWords.forEach( function(word) {
          for(let i=0;i<rowTot;i++){
              for(let j=0;j<colTot;j++){
                  let result = searchWordMatrix(data, i, j, word);
                  if(result.found){
                      console.log(word, " ", result.start.join(":"), " ", result.end.join(":"));
                  }
              }
          }
      });
  });
  
  // 8 possible directions from one point
  const x = [-1, -1, -1, 0, 0, 1, 1, 1];
  const y = [-1, 0, 1, -1, 1, -1, 0, 1];
  
  function searchWordMatrix(data, row, col, word) 
  { 
      // first letter not match
      if (data[row][col] != word[0]) {
          return {found: false}; 
      }
    
      let len = word.length; 
    
      // Search word in 8 directions 
      for (let dir = 0; dir < 8; dir++) { 
          let k, rd = row + x[dir], cd = col + y[dir]; 
    
          for (k = 1; k < len-1; k++) { 
              // If out of bound break 
              if (rd >= rowTot || rd < 0 || cd >= colTot || cd < 0) {
                  break; 
              }
              // Not match
              if (data[rd][cd] != word[k]) {
                  break; 
              }
              // Moving in particular direction 
              rd += x[dir], cd += y[dir]; 
          } 
    
          // All match
          if (k == len-1) {
              return {found: true, start: [row, col], end: [rd, cd]}; 
          }
      } 
      return {found: false}; 
  }